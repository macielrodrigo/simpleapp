//
//  MessagePresenter.swift
//  SimpleApp
//
//  Created by Rodrigo Damacena Gamarra Maciel on 23/03/20.
//  Copyright © 2020 Rodrigo Damacena Gamarra Maciel. All rights reserved.
//

import Foundation

protocol MessagePresenter {
    
    func showErrorMessage(error: String?)
    func showMessage(message: String?, title: String?)
    
}
