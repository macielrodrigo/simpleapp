//
//  MessagePresenter+Extensios.swift
//  SimpleApp
//
//  Created by Rodrigo Damacena Gamarra Maciel on 23/03/20.
//  Copyright © 2020 Rodrigo Damacena Gamarra Maciel. All rights reserved.
//

import UIKit

extension MessagePresenter where Self: UIViewController {
    
    func showErrorMessage(error: String?) {
        showMessage(message: error, title: "Atenção")
    }
    
    func showMessage(message: String?, title: String? = nil) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
