//
//  ResultViewController.swift
//  SimpleApp
//
//  Created by Rodrigo Damacena Gamarra Maciel on 19/03/20.
//  Copyright © 2020 Rodrigo Damacena Gamarra Maciel. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {
    
    private var resultView: ResultView!
    
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = .white
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.resultView = ResultView(frame: UIScreen.main.bounds)
        self.view.addSubview(resultView)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.resultView.animationLabel()
    }
    
}
