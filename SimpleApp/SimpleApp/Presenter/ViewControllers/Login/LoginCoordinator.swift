//
//  LoginCoordinator.swift
//  SimpleApp
//
//  Created by Rodrigo Damacena Gamarra Maciel on 19/03/20.
//  Copyright © 2020 Rodrigo Damacena Gamarra Maciel. All rights reserved.
//

import UIKit


protocol LoginCoordinatorDelegate {
    
    func didSignIn()
}

class LoginCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var loginCoordinatorDelegate: LoginCoordinatorDelegate?
    
    init(navigationController: UINavigationController, loginCoordinatorDelegate: LoginCoordinatorDelegate?) {
        self.navigationController = navigationController
        self.loginCoordinatorDelegate = loginCoordinatorDelegate
    }
     
    func start() {
        let viewController = LoginViewController()
        viewController.delegate = self
        self.navigationController.viewControllers = [viewController]
    }
}

extension LoginCoordinator: LoginViewControllerDelegate {
    func didSignIn() {
        self.loginCoordinatorDelegate?.didSignIn()
    }
}
