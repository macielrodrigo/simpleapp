//
//  LoginViewController.swift
//  SimpleApp
//
//  Created by Rodrigo Damacena Gamarra Maciel on 19/03/20.
//  Copyright © 2020 Rodrigo Damacena Gamarra Maciel. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol LoginViewControllerDelegate {
    
    func didSignIn()
}

class LoginViewController: BaseViewController {
    
    private var loginView: LoginView!
    private var viewModel = LoginViewModel(loginUseCaseProtocol: LoginUseCase())
    var delegate: LoginViewControllerDelegate?
    let disposeBag = DisposeBag()
    
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = .white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loginView = LoginView(frame: UIScreen.main.bounds)
        self.view.addSubview(self.loginView)
        configureTapGesture()
        setTextInTextFields()
        bind()
    }
    
    func setTextInTextFields() {
        self.loginView.emailTextField.text = "mail@mail.com"
        self.loginView.passwordTextField.text = "1234"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
}

extension LoginViewController {
    
    func bind(){
        
        self.loginView.emailTextField.rx.text.orEmpty
            .bind(to: viewModel.email)
            .disposed(by: disposeBag)
        
        self.loginView.passwordTextField.rx.text.orEmpty
            .bind(to: viewModel.password)
            .disposed(by: disposeBag)
        
        self.loginView.enterButton.rx.tap.do(onNext:  { [unowned self] in
            self.loginView.emailTextField.resignFirstResponder()
            self.loginView.passwordTextField.resignFirstResponder()
        }).subscribe(onNext: { [unowned self] in
            self.viewModel.validateCredentials()
        }).disposed(by: disposeBag)
        
        viewModel.isSuccess.asObservable()
            .bind{ value in
                if(value){ self.delegate?.didSignIn()}
        }.disposed(by: disposeBag)
        
        
        viewModel.errorMsg.asObservable()
            .bind { errorMessage in
                if(!errorMessage.isEmpty) {
                    self.showErrorMessage(error: errorMessage)
                }
        }.disposed(by: disposeBag)
    }
}
