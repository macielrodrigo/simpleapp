//
//  LoginView.swift
//  SimpleApp
//
//  Created by Rodrigo Damacena Gamarra Maciel on 19/03/20.
//  Copyright © 2020 Rodrigo Damacena Gamarra Maciel. All rights reserved.
//

import UIKit

class LoginView: UIView {
    
    lazy var emailTextField: UITextField = {
        let t = UITextField()
        t.placeholder = "E-mail"
        t.keyboardType = .emailAddress
        t.clearButtonMode = .whileEditing
        t.autocorrectionType = .no
        t.autocapitalizationType = .none
        t.borderStyle = .none
        t.backgroundColor = .clear
        t.textColor = .black
        t.translatesAutoresizingMaskIntoConstraints = false
        return t
    }()
    
    lazy var passwordTextField: UITextField = {
        let t = UITextField()
        t.placeholder = "Senha"
        t.keyboardType = .default
        t.isSecureTextEntry = true
        t.clearButtonMode = .whileEditing
        t.backgroundColor = .clear
        t.borderStyle = .none
        t.textColor = .black
        t.translatesAutoresizingMaskIntoConstraints = false
        
        return t
    }()
    
    
    lazy var enterButton: UIButton = {
        let b = UIButton(type: .system)
        b.setTitle("ENTRAR", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        b.backgroundColor = .blue
        b.layer.cornerRadius = 6
        b.layer.masksToBounds = true
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }()
    
    private lazy var componentsView: UIView = {
        let v = UIView()
        v.backgroundColor = .clear
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    private lazy var emailView: UIView = {
        let v = UIView()
        v.backgroundColor = .clear
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    private lazy var passwordView: UIView = {
        let v = UIView()
        v.backgroundColor = .clear
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    
    private lazy var emailSeparetorView: UIView = {
        let v = UIView()
        v.backgroundColor = .black
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    private lazy var passwordSeparetorView: UIView = {
        let v = UIView()
        v.backgroundColor = .black
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented - LoginView")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

extension LoginView {
    
    func setupConstraints() {
        
        self.addSubview(self.componentsView)
        self.addSubview(self.enterButton)
        
        let marginsView = self.layoutMarginsGuide
        
        self.componentsView.addSubview(self.emailView)
        self.componentsView.addSubview(self.passwordView)
        
        let marginsComponentsView = self.componentsView.layoutMarginsGuide
        
        NSLayoutConstraint.activate([self.componentsView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
                                     self.componentsView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
                                     self.componentsView.centerXAnchor.constraint(equalTo: marginsView.centerXAnchor),
                                     self.componentsView.centerYAnchor.constraint(equalTo: marginsView.centerYAnchor),
                                     self.componentsView.heightAnchor.constraint(equalToConstant: 110)])
        
        
        NSLayoutConstraint.activate([self.emailView.leadingAnchor.constraint(equalTo: marginsComponentsView.leadingAnchor),
                                     self.emailView.trailingAnchor.constraint(equalTo: marginsComponentsView.trailingAnchor),
                                     self.emailView.topAnchor.constraint(equalTo: marginsComponentsView.topAnchor),
                                     self.emailView.heightAnchor.constraint(equalToConstant: 45)])
        
        NSLayoutConstraint.activate([self.passwordView.topAnchor.constraint(equalTo: emailView.bottomAnchor, constant: 10),
                                     self.passwordView.leadingAnchor.constraint(equalTo: marginsComponentsView.leadingAnchor),
                                     self.passwordView.trailingAnchor.constraint(equalTo: marginsComponentsView.trailingAnchor),
                                     self.passwordView.heightAnchor.constraint(equalToConstant: 45)])
        //
        self.emailView.addSubview(self.emailTextField)
        self.emailView.addSubview(self.emailSeparetorView)
        self.passwordView.addSubview(self.passwordTextField)
        self.passwordView.addSubview(self.passwordSeparetorView)
        //
        NSLayoutConstraint.activate([self.emailTextField.topAnchor.constraint(equalTo: self.emailView.topAnchor),
                                     self.emailTextField.leadingAnchor.constraint(equalTo: self.emailView.leadingAnchor),
                                     self.emailTextField.trailingAnchor.constraint(equalTo: self.emailView.trailingAnchor),
                                     self.emailTextField.heightAnchor.constraint(equalToConstant: 40)])
        
        NSLayoutConstraint.activate([self.emailSeparetorView.topAnchor.constraint(equalTo: self.emailTextField.bottomAnchor),
                                     self.emailSeparetorView.leadingAnchor.constraint(equalTo: self.emailView.leadingAnchor),
                                     self.emailSeparetorView.trailingAnchor.constraint(equalTo: self.emailView.trailingAnchor),
                                     self.emailSeparetorView.heightAnchor.constraint(equalToConstant: 1.8)])
        
        //
        NSLayoutConstraint.activate([self.passwordTextField.topAnchor.constraint(equalTo: self.passwordView.topAnchor),
                                     self.passwordTextField.leadingAnchor.constraint(equalTo: self.passwordView.leadingAnchor),
                                     self.passwordTextField.trailingAnchor.constraint(equalTo: self.passwordView.trailingAnchor),
                                     self.passwordTextField.heightAnchor.constraint(equalToConstant: 40)])
        //
        NSLayoutConstraint.activate([self.passwordSeparetorView.topAnchor.constraint(equalTo: self.passwordTextField.bottomAnchor),
                                     self.passwordSeparetorView.leadingAnchor.constraint(equalTo: self.passwordView.leadingAnchor),
                                     self.passwordSeparetorView.trailingAnchor.constraint(equalTo: self.passwordView.trailingAnchor),
                                     self.passwordSeparetorView.heightAnchor.constraint(equalToConstant: 1.8)])
        
        //enter button
        NSLayoutConstraint.activate([self.enterButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
                                     self.enterButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
                                     self.enterButton.heightAnchor.constraint(equalToConstant: 52.8),
                                     self.enterButton.bottomAnchor.constraint(equalTo: marginsView.bottomAnchor, constant: -5)])
    }
}
