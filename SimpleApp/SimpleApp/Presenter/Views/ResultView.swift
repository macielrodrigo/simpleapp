//
//  ResultView.swift
//  SimpleApp
//
//  Created by Rodrigo Damacena Gamarra Maciel on 19/03/20.
//  Copyright © 2020 Rodrigo Damacena Gamarra Maciel. All rights reserved.
//

import UIKit

class ResultView: UIView {
    
    lazy var descriptionLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        l.numberOfLines = 0
        l.lineBreakMode = .byWordWrapping
        l.textAlignment = .center
        l.textColor = .black
        l.text = "Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan."
        
        return l
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented - LoginView")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

extension ResultView {
    
    func setupConstraints() {
        
        self.addSubview(self.descriptionLabel)
        let marginsView = self.layoutMarginsGuide
        
        NSLayoutConstraint.activate([self.descriptionLabel.leadingAnchor.constraint(equalTo: marginsView.leadingAnchor),
                                     self.descriptionLabel.trailingAnchor.constraint(equalTo: marginsView.trailingAnchor),
                                     self.descriptionLabel.centerYAnchor.constraint(equalTo: marginsView.centerYAnchor),
                                     self.descriptionLabel.centerXAnchor.constraint(equalTo: marginsView.centerXAnchor)])
    }
    
    func animationLabel() {
        UIView.animate(withDuration: 0.8) {[weak self] in
            self?.layoutIfNeeded()
        }
    }
}
