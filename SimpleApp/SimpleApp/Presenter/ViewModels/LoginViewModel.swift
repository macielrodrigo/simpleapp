//
//  LoginViewModel.swift
//  SimpleApp
//
//  Created by Rodrigo Damacena Gamarra Maciel on 19/03/20.
//  Copyright © 2020 Rodrigo Damacena Gamarra Maciel. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct LoginViewModel {
    
    let disposebag = DisposeBag()
    
    let email = BehaviorRelay<String>(value: "")
    let password = BehaviorRelay<String>(value: "")
    
    var isSuccess : BehaviorRelay<Bool> = BehaviorRelay(value: false)
    var errorMsg : BehaviorRelay<String> = BehaviorRelay(value: "")
    
    var loginUseCaseProtocol: LoginUseCaseProtocol
    
    init(loginUseCaseProtocol: LoginUseCaseProtocol) {
        self.loginUseCaseProtocol = loginUseCaseProtocol
    }
    
    func validateCredentials() {
        let login = Login(email: email.value, password: password.value)
        loginUseCaseProtocol.execute(login).subscribe(onNext: { (response) in
            switch response {
            case .success(let value):
                self.isSuccess.accept(value)
            case .failure(let error):
                self.handleBussinessError(error)
            }
        }).disposed(by: disposebag)
    }
    
    private func handleBussinessError(_ error: LoginError) {
        switch error {
        case .email(let error):
            self.handleEmailError(error)
        case .password(let error):
            self.handlePasswordError(error)
        }
    }
    
    private func handleEmailError(_ error: LoginError.Email) {
        switch error {
            
        case .empty:
            errorMsg.accept("E-mail vazio")
        case .invalid:
            errorMsg.accept("E-mail inválido")
        }
    }
    
    private func handlePasswordError(_ error: LoginError.Password) {
        switch error {
            
        case .empty:
            errorMsg.accept("Senha vazia")
        case .invalid:
            errorMsg.accept("Senha inválida")
        case .lenght:
            errorMsg.accept("Senha deve ter quatro ou mais caracteres")
        }
    }
    
}
