//
//  Login.swift
//  SimpleApp
//
//  Created by Rodrigo Damacena Gamarra Maciel on 20/03/20.
//  Copyright © 2020 Rodrigo Damacena Gamarra Maciel. All rights reserved.
//

import Foundation


struct Login {
    
    let email: String?
    let password: String?
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
    }
}
