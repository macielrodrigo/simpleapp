//
//  DefaultLoginUseCase.swift
//  SimpleApp
//
//  Created by Rodrigo Damacena Gamarra Maciel on 20/03/20.
//  Copyright © 2020 Rodrigo Damacena Gamarra Maciel. All rights reserved.
//

import UIKit
import RxSwift

public struct LoginUseCase: LoginUseCaseProtocol {

    
    init() {}
    
    func execute(_ login: Login) -> Observable<Result<Bool>> {
        
        if let email = login.email, let password = login.password {
            
            if (validateEmail(text: email) && validadeLenght(text: password, size: 4)) {
                return Observable.just(.success(true))
            }
            
            if (email.isEmpty)
            {
                return Observable.just(.failure(error: .email(.empty)))
            }
            else if (!validateEmail(text: email))
            {
                return Observable.just(.failure(error: .email(.invalid)))
            }
            if (password.isEmpty)
            {
                return Observable.just(.failure(error: .password(.empty)))
            }
            else if (!validadeLenght(text: password, size: 4))
            {
                return Observable.just(.failure(error: .password(.lenght)))
            }
        }
    
        return Observable.just(.success(false))
    }
    
     func validateEmail(text : String) -> Bool {
           let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
           
           let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
           return emailTest.evaluate(with: text)
    }
    
    func validadeLenght(text: String, size: Int) -> Bool {
        return text.count >= size
    }
}
