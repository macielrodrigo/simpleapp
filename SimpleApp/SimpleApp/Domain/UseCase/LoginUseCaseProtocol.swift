//
//  LoginUseCase.swift
//  SimpleApp
//
//  Created by Rodrigo Damacena Gamarra Maciel on 20/03/20.
//  Copyright © 2020 Rodrigo Damacena Gamarra Maciel. All rights reserved.
//

import Foundation
import RxSwift

protocol LoginUseCaseProtocol {
    func execute(_ login: Login) -> Observable<Result<Bool>>
}
