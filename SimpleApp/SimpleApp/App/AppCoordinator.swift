//
//  AppCoordinator.swift
//  SimpleApp
//
//  Created by Rodrigo Damacena Gamarra Maciel on 19/03/20.
//  Copyright © 2020 Rodrigo Damacena Gamarra Maciel. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    private var window: UIWindow
    
    init(window: UIWindow) {
        
        self.window = window
        self.navigationController = UINavigationController()
        window.rootViewController = self.navigationController
        window.makeKeyAndVisible()
        
    }
      
    func start() {
        self.showLogin()
    }
}

extension AppCoordinator {
    
    func showLogin() {
        let loginCoordinator = LoginCoordinator(navigationController: self.navigationController, loginCoordinatorDelegate: self)
        loginCoordinator.start()
    }
    
    func showResult() {
        let resultCoordinator = ResultCoordinator(navigationController: self.navigationController)
        resultCoordinator.start()
    }
}

extension AppCoordinator: LoginCoordinatorDelegate {
    
    func didSignIn() {
        self.showResult()
    }
}


