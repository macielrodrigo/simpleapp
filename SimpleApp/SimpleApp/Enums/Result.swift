//
//  Result.swift
//  SimpleApp
//
//  Created by Rodrigo Damacena Gamarra Maciel on 20/03/20.
//  Copyright © 2020 Rodrigo Damacena Gamarra Maciel. All rights reserved.
//

import Foundation

enum Result<Value> {
    case success(Value)
    case failure(error: LoginError)
}
