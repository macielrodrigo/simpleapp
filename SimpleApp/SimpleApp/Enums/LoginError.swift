//
//  LoginError.swift
//  SimpleApp
//
//  Created by Rodrigo Damacena Gamarra Maciel on 21/03/20.
//  Copyright © 2020 Rodrigo Damacena Gamarra Maciel. All rights reserved.
//

import Foundation

enum LoginError: Error {
    
    enum Email: Error {
        case empty
        case invalid
    }

    enum Password: Error {
        case empty
        case invalid
        case lenght
    }

    case email(Email)
    case password(Password)
    
}
